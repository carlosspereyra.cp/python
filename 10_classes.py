### Classes ###

"""
Para englosar de principio a fin un objeto
identificar el codigo dentro de un ambito

se define como class Persona:
"""

class EmptyHumano:#se debe escribir el nombre con la primer letra con MAYUSCULA
    pass #nulo 

print(EmptyHumano)

class Humano:
    def __init__(self, nombre, apellido, dni):#constructor de clases
        self.nombre = nombre
        self.apellido = apellido
        self.dni = dni


un_humano = Humano("Carlos\n","Pereyra\n",35487753)
print(f"Nombre:{un_humano.nombre}Apellido:{un_humano.apellido}Dni:{un_humano.dni}");