### Loops ###

#While

mi_condicion = 0

while mi_condicion < 10:
    print(mi_condicion)
    mi_condicion += 3 #va a sumar 3 veces cada vez que sea menor a 10
    if mi_condicion == 6:
        print("Se detuvo")
        break #para que se detenga la ejecucion en el caso de necesitar
else: #se puede poner un else al WHILE, es opcional
    print ("superó el 10")



#For
mi_lista = [10, 20, 30]
mi_tupla = [29, "Carlin", "Carlin", 1.70, "informática"]
mi_set = {"Carlin", 35, 1.70}
mi_dict = {
    "Nombre":"Carlos", 
    "Apellido":"Pereyra",
    "Edad":29, 
    "Lenguajes":{"Python","Php","Java"},#inicializar con {} para agregar varios datos
    1:1.70
    } 

print("Lista:\n")

for element in mi_lista:
    print(element)
print("Tuplas:\n")

for element in mi_tupla:
    print(element)
print("Sets:\n")

for element in mi_set:
    print(element)

print("Dict:\n")    
for element in mi_dict:
    print(element)

print("Imprimiendo los valores del Dict:\n")    
for element in mi_dict.values(): #el values devuelve un valor de una List, no hace falta encerrarlo en parentesis con List()
    print(element)
else:
    print("*Se termino el for del dict*")

#Utilizando el else y break
for element in mi_dict: #devuelve las keys
    print(element)
    if element == "Edad":
        print("Se detiene la ejecución en EDAD")#Poner antes del break
        break #Break corta con la ejecucion del for, y el continue se puede utilizar pero para que prosiga
        
else:#este eslse pertenece al for, pero una vez que hace el break ya no sigue por el ELSE
    print("*Se termino el for del dict*")