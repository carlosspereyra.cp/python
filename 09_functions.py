### Functions ###

"""
para definir una funcion se debe ante poner 'def' nombre_funct(): y tabular
"""

def mi_function ():
    print ("Creando una función")

#llamando a la funcion
mi_function ()
#es necesario para que se ejecute

#Puede recibir codigo tambien

def sum_dos_valores (primer_num, segundo_num):
    suma = primer_num + segundo_num
    print( "Sumando", primer_num, "+", segundo_num, "que es igual a =", suma )
    #VER PORQ NO ANDA print( "Sumando %d + %d, que es igual a = %d", primer_num, segundo_num, suma )


#llamando a la funcion
sum_dos_valores(13,13)
sum_dos_valores("T","E") #SUMA TAMBIEN STRING, pero si se pone numeros como string no los suma, muestra nomas.
sum_dos_valores(1.5, 1.5)#tambièn suma decimales

#multiplicando en la funcion

def func_multip(prim,seg):
    return prim * seg #retornando un valor

result= func_multip(305,5)#guardando el resultado de la funcion

print("El resultado de la función es =",result)#Imprimiendo el resultado

#Imprimiendo desde una variable a una funcion

def print_nombre (nombre, apellido): #puedo darle un valor por defecto
    print(f"Hola, tu nombre es {nombre} de apellido {apellido}?")#ANTEPONER F, otra forma de concatenar datos al imprimir

print_nombre(apellido = "Pereyra", nombre= "Carlos")#OPCIONAL:puedo elegir que campos quiero que se refleje el resultado, deben estar todos los valores

#Imprimiendo muchos parametros de texto
def print_textos(*text): #asignar el * para que imprima muchos
    print(text)

print_textos("Buenos", "días", "América")
print_textos("Buenas noches", "Américaaaaaa")
print_textos("AÑO 2023")
#CON UNA MISMA FUNCION, SE PUEDE IMPRIMIR MUCHOS VALORES

#Imprimiendo muchos parametros de texto EN MAYUSCULA
def print_textos_mayus(*texts): #asignar el * para que imprima muchos
    for text in texts:    
        print(text.upper())
print_textos_mayus("hola","como","estas")