### Diccionarios ###

mi_dict = dict()
mi_otro_dict = {} #tambien se inicializa asi

print(type(mi_dict)) #refleja el tipo de datos
print(type(mi_otro_dict)) #refleja el tipo de datos

mi_otro_dict = {
    "Nombre":"Carlos",
    "Apellido":"Pereyra",
    "Edad":29, 
    1:"Python"} #tambien se inicializa asi
#print(mi_otro_dict) #Estructura que ya nos sirve para relacionar datos, la clave va antes del : y luego el valor de esa clave


mi_dict = {
    "Nombre":"Carlos", 
    "Apellido":"Pereyra",
    "Edad":29, 
    "Lenguajes":{"Python","Php","Java"},#inicializar con {} para agregar varios datos
    1:1.70
    } 


print(mi_otro_dict)
print(mi_dict)


print(len(mi_otro_dict)) #contabiliza la cantidad de variables q tiene
print(len(mi_dict))#que tenga varios valores en una variable, no quiere decir que tenga mas cantidad de datos

print(mi_dict["Nombre"])#facilidad de acceder al dato por medio de diccionarios, devuelve el valor de la clave

#actualizando el dato de la clave

mi_dict["Nombre"] = "Emiliano"
print(mi_dict["Nombre"])#dato actualizado Emiliano, en vez de Carlos

print(mi_dict[1])

mi_dict ["Calle"] = "Calle 130 - 3753" #agregando un dato nuevo al diccionario
print(mi_dict)

del mi_dict["Calle"]#borra solo el dato que le paso
print(mi_dict)

#revisando datos en el diccionario POR CLAVE

print("Nombre" in mi_dict)#true
print("Carlos" in mi_otro_dict)#false

#retornando valores
#listado claves con valores
print(mi_dict.items())
#Solo claves
print(mi_dict.keys())
#Solo valores de claves
print(mi_dict.values())

#pasando las claves de un diccionario a otro nuevo

mi_nuevo_dict = dict.fromkeys(mi_dict) #el nuevo retoma datos de mi_dict
print(mi_nuevo_dict)

#se puede transformar un diccionario en una lista, retomando claves de otro diccionario

print(list(dict.fromkeys(list(mi_nuevo_dict.values())).keys()))
