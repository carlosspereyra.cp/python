"""
mivariable_string = "hola mundo"
mivariable_int = 33
mivariable_bool = False

print(mivariable_string, mivariable_int, mivariable_bool)

nombre, apellido, edad, apodo = "Carlos", "Pereyra", 29, "Carlin"

print("Hola mi nombre es", nombre, apellido," Tengo ", edad, "años"
" y me dicen por mi apodo que es", apodo, ".Mucho gusto!!")
 

Con el input podes ingresar valores, preguntando al usuario primero.
anteponiendo la variable= input...

nombre = input("¿Como era tu nombre?")
print("Ah, mucho gusto",nombre, "!!")

apellido = input("¿Como era tu apelido?")
print("Listo, anotado en mi agenda",nombre, apellido,"!!")

#edad = input("¿y tu edad?")
#Para imprimir una frase enterarob

print("el nombre completo entonces %s %s y la edad %d" %(nombre, apellido, edad))
"""
#Imprimiendo caracter por caracter

languaje = "Python"
a, b, c , d ,e ,f = languaje #se debe poner la cantidad exacta de caracteres q usa la frase, en este caso 6
print(a, b, c, d, e, f) #imprime uno al lado del otro
print(a)#imprimiendo uno abajo del otro ___
print(b)
print(c)
print(d)
print(e)
print(f)

languaje_slice= languaje[0:6] #imprime desde el caracter 0 al 6, al caracter que se le asigne, si se le pone solo un nro, va desde ese nro en adelante
print(languaje_slice)

#para imprimir alrevez
reverse_lang = languaje [::-1]
print(reverse_lang)